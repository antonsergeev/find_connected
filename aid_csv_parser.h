#pragma once

#include <cstdio>
#include <vector>

inline bool is_delimiter(const char delimiter) // faster than boost::algorithm::any_of_equal
{
    return delimiter == '\n' || delimiter == '\r' || delimiter == '\0';
}

class AidCsvParser
{
public:
    AidCsvParser(FILE * input_csv): input_csv_{ input_csv }
    {
        constexpr size_t size{ 1'000'000 };
        buffer_.reserve(size);
        buffer_.resize(size);
    };

    template<typename LineHandler>
    void parse_aids(LineHandler&& line_handler)
    {
        bool skip_header = true;
        for (;;)
        {
            size_t const sz_read = load();
            if (sz_read == 0)
                break;
            char* start = buffer_.data();
            const char* end = start + sz_read;
            if (skip_header)
            {
                while (!is_delimiter(*start) && start != end)
                    ++start;
                skip_header = false;
            }

            for (;;)
            {
                while (start != end && is_delimiter(*start++));
                if (start == end)
                    break;
                for (int i = 0; i != 2; ++i)
                {
                    for (; start != end && *start != ','; ++start);
                    start != end && ++start;
                }
                bool single = true;
                for (; !is_delimiter(*start);)
                {
                    const unsigned input = strtoul(start, &start, 10);
                    single = single && *start != '|';
                    if (single)
                        continue;
                    line_handler(input);
                    if (*start == '|')
                        ++start;
                }
                line_handler.end_line();
            }
        }
    }
private:
    inline size_t load() // to read_blob/block
    {
        size_t size = std::fread(buffer_.data(), sizeof(char), buffer_.size() - 1, input_csv_);
        if (!size)
            return 0;
        if (!std::feof(input_csv_))
        {
            size_t last = size - 1;
            for (; last != 0 && !is_delimiter(buffer_[last]); --last);
            if (last != size - 1)
            {
#ifdef _MSC_VER
                _fseeki64(input_csv_, -static_cast<long long>(size - 1 - last), SEEK_CUR);
#else
                fseeko(input_csv_, -static_cast<long long>(size - 1 - last), SEEK_CUR);
#endif // _MSC_VER
                size = last + 1;
            }
        }
        buffer_[size++] = '\0';
        return size;
    }

    FILE * input_csv_;
    std::vector<char> buffer_;
};