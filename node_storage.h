#pragma once

#include <algorithm>
#include <vector>

#include "node.h"

class NodeStorage
{
public:
    template< typename Range >
    NodeStorage(Range const& range)
    {
        const size_t sz = range.size();
        storage_.reserve(sz);
        unsigned index = 0;
        for (const auto id : range)
            storage_.push_back({ index++, empty_index, id, empty_index });
    }
    inline unsigned get_index(node* ptr)
    {
        return static_cast<unsigned>(ptr - get_ptr(0));
    }
    inline node* get_ptr(unsigned index)
    {
        return &storage_[index];
    }
    inline const node* get_ptr(unsigned index) const
    {
        return &storage_[index];
    }
    node* find(unsigned id)
    {
        node* ptr = &(*std::lower_bound(storage_.begin(), storage_.end(), node{ 0, 0, id }));
        if (ptr->id == id)
            return ptr;
        return nullptr;
    }

    template< typename Handler>
    void for_each(Handler&& handler)
    {
        for (node& current : storage_)
            handler(&current);
    }

private:
    std::vector<node> storage_;
};