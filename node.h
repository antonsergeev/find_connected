#pragma once

#include <utility>

struct node
{
    unsigned next_in_list;
    unsigned parent;
    unsigned id;
    unsigned initial_parent;
};

bool operator < (node const& lhs, node const& rhs)
{
    return lhs.id < rhs.id;
}

namespace std {
template<>
struct hash<node>
{
    size_t operator()(node const& arg) const
    {
        return hash<unsigned>()(arg.id);
    }
};
}

bool operator ==(node const& lhs, node const& rhs)
{
    return lhs.id == rhs.id;
}

void join_list(node * lhs, node * rhs)
{
    std::swap(lhs->next_in_list, rhs->next_in_list);
}

static constexpr unsigned empty_index = static_cast<unsigned>(-1);