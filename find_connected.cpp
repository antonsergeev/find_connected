#include <algorithm>
#include <chrono>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <iterator>
#include <map>
#include <set>
#include <string>
#include <type_traits>
#include <vector>

#include "aid_csv_parser.h"
#include "elapsed_time.h"
#include "node.h"
#include "node_storage.h"

class Forest
{
public:
    template< typename Range >
    Forest(Range const& range)
    : node_storage_{ range }
    {}

    template< typename Handler>
    void traverse(const node* requested, Handler&& handler)
    {
        for (node* current = node_storage_.get_ptr(requested->next_in_list); current != requested; current = node_storage_.get_ptr(current->next_in_list))
        {
            handler(current);
        }
    }

    template< typename Handler>
    void for_each(Handler&& handler)
    {
        node_storage_.for_each(std::forward<Handler>(handler));
    }

    node* compress(node* current_node)
    {
        node* root_node = current_node;
        for (; root_node->parent != empty_index; root_node = node_storage_.get_ptr(root_node->parent));
        if (current_node != root_node)
        {
            unsigned const root = node_storage_.get_index(root_node);
            for (node* parent = node_storage_.get_ptr(current_node->parent); parent != root_node; )
            {
                current_node->parent = root;
                current_node = parent;
                parent = node_storage_.get_ptr(parent->parent);
            }
        }
        return root_node;
    }

    node* find(unsigned id)
    {
        return node_storage_.find(id);
    }

    void join_trees(node* child, node * root)
    {
        if (child == root)
            return;
        child->parent = child->initial_parent = node_storage_.get_index(root);
        join_list(child, root);
    }

    void detach(node* child)
    {
        child->initial_parent = empty_index;
        traverse(child, [](node* current){current->parent = current->initial_parent;});
    }

private:
    NodeStorage node_storage_;
};

using IdSet = std::vector<unsigned>;

class IdSetReader
{
public:
    IdSetReader(IdSet& id_set)
    : id_set_{ id_set }
    {}
    void operator()(unsigned id)
    {
        id_set_.push_back(id);
    }
    void end_line() {}
private:
    IdSet& id_set_;
};

class TreeBuilder
{
public:
    TreeBuilder(Forest& forest)
    : forest_(forest)
    { }
    void operator()(unsigned id)
    {
        node * root_for_id = forest_.compress(forest_.find(id));
        if (root == nullptr)
            root = root_for_id;
        else
            forest_.join_trees(root_for_id, root);
    }
    void end_line()
    {
        root = nullptr;
    }
private:
    node * root = nullptr;
    Forest& forest_;
};

class Verifier
{
public:
    Verifier(std::set<unsigned>const& check) : check_(check){}
    void operator()(unsigned id)
    {
        if(matched == -1)
            matched = check_.count(id);
        else
            if(matched != check_.count(id))
                throw;
    }
    void end_line()
    {
        matched = -1;
    }
private:
    size_t matched;
    std::set<unsigned> const& check_;
};

void compress_and_log_roots(Forest& forest, std::string const& filename)
{
    std::ofstream out(filename);
    std::map<unsigned, unsigned> count_;
    forest.for_each([&forest, &count_] (node* current) { ++count_[forest.compress(current)->id]; });

    std::vector<std::pair<unsigned, unsigned> > result;
    result.reserve(count_.size());
    for (auto const& p : count_)
    {
        result.push_back({ p.second, p.first });
    }
    std::sort(result.begin(), result.end());
    for (auto const& p : result)
    {
        out << p.second << ' ' << p.first << '\n';
    }
}

void verify_result(Forest& forest, unsigned requested_id, FILE* input_csv)
{
    std::cout << "Result verification..." << std::endl;
    std::set<unsigned> tree_items;

    tree_items.insert(requested_id);
    const node* requested = forest.find(requested_id);
    forest.traverse(requested, [&tree_items] (const node* current)
    {
        tree_items.insert(current->id);
    });

    try
    {
        std::rewind(input_csv);
        AidCsvParser(input_csv).parse_aids(Verifier(tree_items));
        std::cout << "Succeeded!" << std::endl;
    }
    catch (...)
    {
        std::cout << "Verification failed!" << std::endl;
    }
}

void sort_unique(IdSet& id_set)
{
    std::sort(id_set.begin(), id_set.end());
    id_set.erase(std::unique(id_set.begin(), id_set.end()), id_set.end());
    id_set.shrink_to_fit();
}

int main( int argc, const char* argv[] ){
    ElapsedTime total_time;
    if(argc < 2)
    {
        std::cout << "Please, specify input file." << std::endl;
        return 1;
    }

    FILE * input_csv = fopen(argv[1], "rb");
    std::cout << "Parsing AIDs." << std::endl;
    IdSet id_set;
    try
    {
        constexpr size_t expected_size = 600'000'000;
        id_set.reserve(expected_size);
    }
    catch (std::bad_alloc const&)
    {
        std::cout << "Warning: low memory!" << std::endl;
    }
    
    AidCsvParser(input_csv).parse_aids(IdSetReader(id_set));
    std::cout << "Elapsed: " << total_time << std::endl;
    std::cout << "Sorting AIDs." << std::endl;
    sort_unique(id_set);
    std::cout << "Elapsed: " << total_time << std::endl;

    Forest forest(id_set);
    id_set.clear();
    id_set.shrink_to_fit();

    std::rewind(input_csv);

    std::cout << "Parsing second time and building forest." << std::endl;
    AidCsvParser(input_csv).parse_aids(TreeBuilder(forest));

    std::cout << "Total time: " << total_time << std::endl;

    std::string const verify_option = "--verify";
    const bool verification_enabled = argc >= 3 && argv[2] == verify_option;

    std::cout << "> " << std::endl;
    for (std::string line; std::getline(std::cin, line);)
    {
        char* start = const_cast<char*>(line.c_str());
        const unsigned requested_id = strtoul(start, &start, 10);
        if(start == line.c_str())
            break;
        
        ElapsedTime request_handling_time;

        const node* requested = forest.find(requested_id);
        if (requested == nullptr)
        {
            std::cout << "No connected address Ids found!" << std::endl;
            continue;
        }

        {
            int count = 1;
            forest.traverse(requested, [&count]( auto const& ){ ++count; });
            std::cout << "Request handling time (" << count << " items): " << request_handling_time << std::endl;
        }
        {
            ElapsedTime output_print_time;
            std::ofstream out(line + ".txt");
            forest.traverse(requested, [&out] (const node* current){ out << current->id << '\n'; });
            std::cout << "With file output: " << output_print_time << std::endl;
        }
        
        if(verification_enabled)
            verify_result(forest, requested_id, input_csv);
        
        std::cout << "> " << std::endl;
    }

    if (argc >= 3)
    {
        compress_and_log_roots(forest, argc == 3 ? argv[2] : argv[3]);
    }

    return 0;
}
